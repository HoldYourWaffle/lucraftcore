package lucraft.mods.lucraftcore.utilities.blocks;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.util.helper.ItemHelper;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class UtilitiesBlocks {

	public static Block CONSTRUCTION_TABLE = new BlockConstructionTable();
	
	@SubscribeEvent
	public void onRegisterBlocks(RegistryEvent.Register<Block> e) {
		e.getRegistry().register(CONSTRUCTION_TABLE);
	}
	
	@SubscribeEvent
	public void onRegisterItems(RegistryEvent.Register<Item> e) {
		e.getRegistry().register(new ItemBlock(CONSTRUCTION_TABLE).setRegistryName(CONSTRUCTION_TABLE.getRegistryName()));
	}
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void onRegisterModels(ModelRegistryEvent e) {
		ItemHelper.registerItemModel(Item.getItemFromBlock(CONSTRUCTION_TABLE), LucraftCore.MODID, "construction_table");
	}
	
}
