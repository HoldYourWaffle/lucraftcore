package lucraft.mods.lucraftcore.superpowers.suitsets;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityGenerator;
import lucraft.mods.lucraftcore.superpowers.effects.Effect;
import lucraft.mods.lucraftcore.superpowers.effects.EffectHandler;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import lucraft.mods.lucraftcore.util.creativetabs.CreativeTabRegistry;
import lucraft.mods.lucraftcore.util.helper.ItemHelper;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.EntityEquipmentSlot.Type;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.*;

public class JsonSuitSet extends SuitSet {

	protected Item helmet;
	protected Item chestplate;
	protected Item legs;
	protected Item boots;

	public JsonObject jsonOriginal;
	public ResourceLocation loc;
	public ArmorMaterial material;
	public ITextComponent name;
	public ITextComponent desc;
	public Map<EntityEquipmentSlot, JsonSuitSetSlotInfo> slotInfos;
	public List<AbilityGenerator> abilities;
	public List<Effect> effects;
	public NBTTagCompound data;
	public String tab;

	public JsonSuitSet(String name) {
		super(name);
	}
	
	@Override
	public String getUnlocalizedName() {
		return loc.toString();
	}

	public void registerItems(Register<Item> e) {
		if (slotInfos.containsKey(EntityEquipmentSlot.HEAD))
			e.getRegistry().register(this.helmet = new ItemSuitSetArmor(this, EntityEquipmentSlot.HEAD).setRegistryName(loc.getResourceDomain(), loc.getResourcePath() + "_head"));

		e.getRegistry().register(this.chestplate = new ItemSuitSetArmor(this, EntityEquipmentSlot.CHEST).setRegistryName(loc.getResourceDomain(), loc.getResourcePath() + "_chest"));

		if (slotInfos.containsKey(EntityEquipmentSlot.LEGS))
			e.getRegistry().register(this.legs = new ItemSuitSetArmor(this, EntityEquipmentSlot.LEGS).setRegistryName(loc.getResourceDomain(), loc.getResourcePath() + "_legs"));

		if (slotInfos.containsKey(EntityEquipmentSlot.FEET))
			e.getRegistry().register(this.boots = new ItemSuitSetArmor(this, EntityEquipmentSlot.FEET).setRegistryName(loc.getResourceDomain(), loc.getResourcePath() + "_feet"));

		CreativeTabRegistry.getOrCreateCreativeTab(this.tab, new ItemStack(getChestplate()));
	}

	@SideOnly(Side.CLIENT)
	public void registerModels() {
		registerModel(getHelmet(), EntityEquipmentSlot.HEAD);
		registerModel(getChestplate(), EntityEquipmentSlot.CHEST);
		registerModel(getLegs(), EntityEquipmentSlot.LEGS);
		registerModel(getBoots(), EntityEquipmentSlot.FEET);
	}

	@SideOnly(Side.CLIENT)
	public void registerModel(Item item, EntityEquipmentSlot slot) {
		if (item != null && slotInfos.get(slot).itemModel != null && !slotInfos.get(slot).itemModelInfo.isEmpty()) {
			JsonSuitSetItemModelType type = slotInfos.get(slot).itemModel;
			String s = slotInfos.get(slot).itemModelInfo;
			if (type == JsonSuitSetItemModelType.MODEL) {
				ResourceLocation loc = new ResourceLocation(s);
				ItemHelper.registerItemModel(item, loc.getResourceDomain(), loc.getResourcePath());
			}
			if (type == JsonSuitSetItemModelType.BLOCKSTATE)
				ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(s), slot.toString().toLowerCase()));
		}
	}

	@Override
	public String getDisplayName() {
		return name.getFormattedText();
	}

	@Override
	public String getDisplayNameForItem(Item item, ItemStack stack, EntityEquipmentSlot armorType, String origName) {
		return slotInfos.get(armorType).name.getFormattedText();
	}

	@Override
	public List<String> getExtraDescription(ItemStack stack) {
		return desc == null ? null : Arrays.asList(desc.getFormattedText());
	}

	@Override
	public String getModId() {
		return loc.getResourceDomain();
	}

	@Override
	public ArmorMaterial getArmorMaterial(EntityEquipmentSlot slot) {
		return material;
	}

	@Override
	public boolean canOpenArmor(EntityEquipmentSlot slot) {
		return slotInfos.get(slot).openable;
	}

	@Override
	public boolean showInCreativeTab() {
		return !tab.equalsIgnoreCase("none");
	}

	@Override
	public CreativeTabs getCreativeTab() {
		return CreativeTabRegistry.getCreativeTab(tab);
	}
	
	@Override
	public float getArmorModelScale(EntityEquipmentSlot armorSlot) {
		return slotInfos.get(armorSlot).armor_model_scale;
	}

	@Override
	public String getArmorTexturePath(ItemStack stack, Entity entity, EntityEquipmentSlot slot, boolean light, boolean smallArms, boolean open) {
		JsonSuitSetSlotInfo info = slotInfos.get(slot);
		String key = light ? "glow" : "normal";
		if (smallArms && info.textures.containsKey(key + "_smallarms"))
			key = key + "_smallarms";
		if (open && info.textures.containsKey(key + "_open"))
			key = key + "_open";

		return info.textures.containsKey(key) ? info.textures.get(key) : "";
	}

	@Override
	protected List<Ability> addDefaultAbilities(EntityPlayer player, List<Ability> list) {
		for (AbilityGenerator ab : abilities) {
			Ability ability = ab.create(player, SuperpowerHandler.getSuitSetAbilityHandler(player));
			if(ability != null)
				list.add(ability);
		}
		return super.addDefaultAbilities(player, list);
	}
	
	@Override
	public Ability getSuitAbilityForKey(AbilityKeys key, List<Ability> list) {
		for(Ability ab : list) {
			if(key.id == ab.getKey()) {
				return ab;
			}
		}
		return super.getSuitAbilityForKey(key, list);
	}

	@Override
	public Item getHelmet() {
		return helmet;
	}

	@Override
	public Item getChestplate() {
		return chestplate;
	}

	@Override
	public Item getLegs() {
		return legs;
	}

	@Override
	public Item getBoots() {
		return boots;
	}
	
	@Override
	public List<Effect> getEffects() {
		return this.effects;
	}
	
	@Override
	public NBTTagCompound getData() {
		return data;
	}

	public JsonSuitSet deserialize(JsonObject jsonobject, ResourceLocation loc) throws Exception {
		JsonObject armor_material = JsonUtils.getJsonObject(jsonobject, "armor_material");
		JsonArray damage_reduction = JsonUtils.getJsonArray(armor_material, "damage_reduction_amounts");
		ArmorMaterial material = EnumHelper.addArmorMaterial(loc.toString(), "", JsonUtils.getInt(armor_material, "durability"), new int[] { damage_reduction.get(0).getAsInt(), damage_reduction.get(1).getAsInt(), damage_reduction.get(2).getAsInt(), damage_reduction.get(3).getAsInt() }, JsonUtils.getInt(armor_material, "enchantibility"), SoundEvent.REGISTRY.getObject(new ResourceLocation(JsonUtils.getString(armor_material, "sound"))), JsonUtils.getFloat(armor_material, "toughness"));

		ITextComponent name = ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(jsonobject, "name").toString());
		ITextComponent desc = null;
		if (JsonUtils.hasField(jsonobject, "description"))
			desc = ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(jsonobject, "description").toString());

		this.name = name;
		this.loc = loc;
		this.desc = desc;
		this.material = material;
		this.slotInfos = new HashMap<>();
		this.abilities = new ArrayList<>();
		this.effects = new ArrayList<>();
		this.tab = JsonUtils.getString(jsonobject, "creative_tab", "addon_packs");

		for (EntityEquipmentSlot slot : EntityEquipmentSlot.values()) {
			if (slot.getSlotType() == Type.ARMOR) {
				if (slot == EntityEquipmentSlot.CHEST && !JsonUtils.hasField(JsonUtils.getJsonObject(jsonobject, "armor_parts"), slot.toString().toLowerCase()))
					throw new Exception("Suit sets need a chestplate!");
				
				if(JsonUtils.hasField(JsonUtils.getJsonObject(jsonobject, "armor_parts"), slot.toString().toLowerCase())) {
					JsonSuitSetSlotInfo info = new JsonSuitSetSlotInfo();
					JsonObject armor = JsonUtils.getJsonObject(JsonUtils.getJsonObject(jsonobject, "armor_parts"), slot.toString().toLowerCase());

					info.openable = JsonUtils.getBoolean(armor, "openable", false);
					info.glow = JsonUtils.getBoolean(armor, "glow", false);
					info.armor_model_scale = JsonUtils.getFloat(armor, "armor_model_scale", slot == EntityEquipmentSlot.HEAD ? 0.5F : (slot == EntityEquipmentSlot.LEGS ? 0.25F : 0.22F));
					info.name = ITextComponent.Serializer.jsonToComponent(JsonUtils.getJsonObject(armor, "name").toString());

					info.textures = new HashMap<>();
					for (Map.Entry<String, JsonElement> entry : JsonUtils.getJsonObject(armor, "textures").entrySet()) {
						info.textures.put(entry.getKey(), entry.getValue().getAsString());
					}

					info.itemModel = null;

					if (JsonUtils.hasField(armor, "item_texture")) {
						JsonObject item_texture = JsonUtils.getJsonObject(armor, "item_texture");
						if (JsonUtils.hasField(item_texture, "model")) {
							info.itemModel = JsonSuitSetItemModelType.MODEL;
							info.itemModelInfo = JsonUtils.getString(item_texture, "model");
						} else if (JsonUtils.hasField(item_texture, "blockstate")) {
							info.itemModel = JsonSuitSetItemModelType.BLOCKSTATE;
							info.itemModelInfo = JsonUtils.getString(item_texture, "blockstate");
						}
					}

					this.slotInfos.put(slot, info);
				}
			}
		}

		if (JsonUtils.hasField(jsonobject, "abilities")) {
			JsonArray array = JsonUtils.getJsonArray(jsonobject, "abilities");
			for (int i = 0; i < array.size(); i++) {
				JsonObject obj = array.get(i).getAsJsonObject();
				ResourceLocation abLoc = new ResourceLocation(JsonUtils.getString(obj, "ability"));
				this.abilities.add(new AbilityGenerator(abLoc, obj, JsonUtils.getInt(obj, "key", 0)));
			}
		}

		if(JsonUtils.hasField(jsonobject, "effects")) {
			JsonArray array = JsonUtils.getJsonArray(jsonobject, "effects");
			for (int i = 0; i < array.size(); i++) {
				JsonObject obj = array.get(i).getAsJsonObject();
				this.effects.add(EffectHandler.makeEffect(obj));
			}
		}
		
		if(JsonUtils.hasField(jsonobject, "data")) {
			String s = JsonUtils.getJsonObject(jsonobject, "data").toString();
			this.data = JsonToNBT.getTagFromJson(s);
		}
		
		return this;
	}

	public static class JsonSuitSetSlotInfo {

		public boolean openable;
		public boolean glow;
		public float armor_model_scale;
		public ITextComponent name;
		public Map<String, String> textures;
		public JsonSuitSetItemModelType itemModel;
		public String itemModelInfo;

	}

	public static enum JsonSuitSetItemModelType {

		MODEL, BLOCKSTATE;

	}

}
