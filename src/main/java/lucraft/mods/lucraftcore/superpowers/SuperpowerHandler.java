package lucraft.mods.lucraftcore.superpowers;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower.SuitSetAbilityHandler;
import lucraft.mods.lucraftcore.superpowers.capabilities.ISuperpowerCapability;
import lucraft.mods.lucraftcore.superpowers.events.PlayerSuperpowerEvent.PlayerGetsSuperpowerEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;

@EventBusSubscriber(modid = LucraftCore.MODID)
public class SuperpowerHandler {

	public static IForgeRegistry<Superpower> SUPERPOWER_REGISTRY;

	@SubscribeEvent
	public static void onRegisterNewRegistries(RegistryEvent.NewRegistry e) {
		SUPERPOWER_REGISTRY = new RegistryBuilder<Superpower>().setName(new ResourceLocation(LucraftCore.MODID, "superpower")).setType(Superpower.class).setIDRange(0, 512).create();
	}

	public static ISuperpowerCapability getSuperpowerData(EntityPlayer player) {
		return player.getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null);
	}

	/**
	 * Sets the player's superpower without paying attention to the current one.
	 * 
	 * @param player
	 * @param superpower
	 */
	public static void setSuperpower(EntityPlayer player, Superpower superpower) {
		if (superpower != null && !SUPERPOWER_REGISTRY.containsValue(superpower)) {
			LucraftCore.LOGGER.error("That superpower isn't registered!");
			return;
		}

		getSuperpowerData(player).setSuperpower(superpower);
		getSuperpowerData(player).loadSuperpowerHandler();
		if(hasSuperpower(player))
			getSuperpowerPlayerHandler(player).onApplyPower();
		syncToAll(player);
	}

	/**
	 * Sets the player's superpower if the current superpower allows to be
	 * overriden/removed.
	 * 
	 * @param player
	 * @param superpower
	 */
	public static void giveSuperpower(EntityPlayer player, Superpower superpower) {
		if (MinecraftForge.EVENT_BUS.post(new PlayerGetsSuperpowerEvent(player, superpower)))
			return;

		if (hasSuperpower(player) && !getSuperpowerPlayerHandler(player).canRemove(superpower))
			return;

		setSuperpower(player, superpower);
	}

	public static void removeSuperpower(EntityPlayer player) {
		setSuperpower(player, null);
	}

	public static Superpower getSuperpower(EntityPlayer player) {
		return getSuperpowerData(player).getSuperpower();
	}

	public static boolean hasSuperpower(EntityPlayer player) {
		return getSuperpower(player) != null;
	}

	public static boolean hasSuperpower(EntityPlayer player, Superpower superpower) {
		return getSuperpower(player) == superpower;
	}

	public static SuperpowerPlayerHandler getSuperpowerPlayerHandler(EntityPlayer player) {
		return getSuperpowerData(player).getSuperpowerPlayerHandler();
	}

	/**
	 * 
	 * @param player
	 * @param handlerClass
	 *            Class the SuperpowerPlayerHandler is an instance of
	 * @return SuperpowerPlayerHandler which is an instance of the given class.
	 *         If it doesn't exist returns null
	 */
	@SuppressWarnings("unchecked")
	public static <T extends SuperpowerPlayerHandler> T getSpecificSuperpowerPlayerHandler(EntityPlayer player, Class<T> handlerClass) {
		SuperpowerPlayerHandler handler = getSuperpowerPlayerHandler(player);
		return (T) (handler == null ? null : (handler.getClass() == handlerClass ? handler : null));
	}

	public static SuitSetAbilityHandler getSuitSetAbilityHandler(EntityPlayer player) {
		return getSuperpowerData(player).getSuitSetAbilityHandler();
	}

	/**
	 * Syncs the superpower data only to the player itself.
	 * 
	 * @param player
	 *            The player that is about to get information about his
	 *            superpower.
	 */
	public static void syncToPlayer(EntityPlayer player) {
		getSuperpowerData(player).syncToPlayer();
	}

	/**
	 * Syncs the superpower data to the given receiver.
	 * 
	 * @param player
	 *            The player whose superpower data will be sent.
	 * @param receiver
	 *            The player that is about to get information about the
	 *            superpower.
	 */
	public static void syncToPlayer(EntityPlayer player, EntityPlayer receiver) {
		getSuperpowerData(player).syncToPlayer(receiver);
	}

	/**
	 * Syncs the superpower data to all players.
	 * 
	 * @param player
	 *            The player whose superpower data will be sent to everyone.
	 */
	public static void syncToAll(EntityPlayer player) {
		getSuperpowerData(player).syncToAll();
	}

}
