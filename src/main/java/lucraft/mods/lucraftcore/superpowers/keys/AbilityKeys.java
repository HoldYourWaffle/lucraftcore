package lucraft.mods.lucraftcore.superpowers.keys;

public enum AbilityKeys {

	ARMOR_1(AbilityKeyType.ARMOR_ACTION, 1),
	ARMOR_2(AbilityKeyType.ARMOR_ACTION, 2),
	ARMOR_3(AbilityKeyType.ARMOR_ACTION, 3),
	ARMOR_4(AbilityKeyType.ARMOR_ACTION, 4),
	ARMOR_5(AbilityKeyType.ARMOR_ACTION, 5),
	SUPERPOWER_1(AbilityKeyType.SUPERPOWER_ACTION, 1),
	SUPERPOWER_2(AbilityKeyType.SUPERPOWER_ACTION, 2),
	SUPERPOWER_3(AbilityKeyType.SUPERPOWER_ACTION, 3),
	SUPERPOWER_4(AbilityKeyType.SUPERPOWER_ACTION, 4),
	SUPERPOWER_5(AbilityKeyType.SUPERPOWER_ACTION, 5);

	public final AbilityKeyType type;
	public final int id;
	
	private AbilityKeys(AbilityKeyType type, int id) {
		this.type = type;
		this.id = id;
	}
	
	public static enum AbilityKeyType {
		ARMOR_ACTION, SUPERPOWER_ACTION;
	}
	
}
