package lucraft.mods.lucraftcore.superpowers.effects;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.minecraft.util.JsonUtils;
import net.minecraft.util.math.Vec3d;

public class EffectGlow extends Effect {

	public Vec3d color;
	public float size;
	
	@Override
	public void readSettings(JsonObject json) {
		JsonArray array = JsonUtils.getJsonArray(json, "color");
		this.color = new Vec3d(array.get(0).getAsDouble(), array.get(1).getAsDouble(), array.get(2).getAsDouble());
		this.size = JsonUtils.getFloat(json, "size", 1F);
	}

}
