package lucraft.mods.lucraftcore.superpowers.effects;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.math.Vec3d;

public class EffectFlickering extends Effect {

	public Vec3d color;
	public boolean useSuitFlicker;
	
	@Override
	public void readSettings(JsonObject json) {
		JsonArray array = JsonUtils.getJsonArray(json, "color");
		this.color = new Vec3d(array.get(0).getAsDouble(), array.get(1).getAsDouble(), array.get(2).getAsDouble());
		
		this.useSuitFlicker = JsonUtils.getBoolean(json, "use_suit_flicker");
	}
	
	public Vec3d getColor(EntityPlayer player) {
		if(!useSuitFlicker)
			return color;
		else {
			SuitSet suitSet = SuitSet.getSuitSet(player);
			if(suitSet != null && suitSet.getData() != null && suitSet.getData().hasKey("flicker")) {
				NBTTagCompound data = suitSet.getData().getCompoundTag("flicker");
				return new Vec3d(data.getDouble("red"), data.getDouble("green"), data.getDouble("blue"));
			}
			
			return color;
		}
	}
	
}
