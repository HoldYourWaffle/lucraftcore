package lucraft.mods.lucraftcore.superpowers.effects;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;

public abstract class Effect {

	public List<EffectCondition> conditions = new ArrayList<>();
	
	public abstract void readSettings(JsonObject json);
	
}
