package lucraft.mods.lucraftcore.superpowers.effects;

import com.google.gson.JsonObject;

import net.minecraft.entity.player.EntityPlayer;

public abstract class EffectCondition {

	public abstract boolean isFulFilled(EntityPlayer player);
	
	public abstract void readSettings(JsonObject json);
	
}
