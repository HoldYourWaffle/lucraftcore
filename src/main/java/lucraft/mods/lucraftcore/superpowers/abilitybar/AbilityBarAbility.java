package lucraft.mods.lucraftcore.superpowers.abilitybar;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.IAbilityContainer;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeyBindings;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.player.EntityPlayer;

public class AbilityBarAbility {

	public Ability ability;
	public AbilityOrigin origin;

	public AbilityBarAbility(Ability ability, AbilityOrigin origin) {
		this.ability = ability;
		this.origin = origin;
	}

	public boolean isActive(Minecraft mc, EntityPlayer player, Superpower superpower, IAbilityContainer abilities) {
		return ability != null && ability.isUnlocked();
	}
	
	public void render(Minecraft mc, int x, int y, EntityPlayer player, Superpower superpower, IAbilityContainer abilities) {
		ability.drawIcon(mc, mc.ingameGUI, x, y);
		GlStateManager.translate(0, 0, 1);
		ability.drawAdditionalInfo(mc, mc.ingameGUI, x, y);
		GlStateManager.translate(0, 0, -1);
	}

	public AbilityKeys getKeyFromAbility(EntityPlayer player, IAbilityContainer abilities) {
		for (AbilityKeys keys : AbilityKeys.values()) {
			if (ability == abilities.getAbilityForKey(keys)) {
				return keys;
			}
		}

		return null;
	}

	public int getLength(Minecraft mc, EntityPlayer player, Superpower superpower, IAbilityContainer abilities) {
		return ability.renderCooldown() ? 25 : 21;
	}
	
	public String getKeyInfo(Minecraft mc, EntityPlayer player, Superpower superpower, IAbilityContainer abilities) {
		AbilityKeys key = getKeyFromAbility(player, abilities);
		if(key != null) {
			return GameSettings.getKeyDisplayString(AbilityKeyBindings.getKeyBindingFromKeyType(key).getKeyCode());
		}
		return null;
	}
	
	public static enum AbilityOrigin {
		
		SUPERPOWER, SUIT;
		
	}
	
}