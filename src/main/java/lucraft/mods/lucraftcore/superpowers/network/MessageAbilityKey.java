package lucraft.mods.lucraftcore.superpowers.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability.AbilityType;
import lucraft.mods.lucraftcore.superpowers.events.AbilityKeyEvent;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys.AbilityKeyType;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import lucraft.mods.lucraftcore.util.triggers.LCCriteriaTriggers;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageAbilityKey implements IMessage {

	public boolean pressed;
	public AbilityKeys key;

	public MessageAbilityKey() {
	}

	public MessageAbilityKey(boolean pressed, AbilityKeys key) {
		this.pressed = pressed;
		this.key = key;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.pressed = buf.readBoolean();
		this.key = AbilityKeys.values()[buf.readInt()];
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(this.pressed);
		buf.writeInt(this.key.ordinal());
	}

	public static class Handler extends AbstractServerMessageHandler<MessageAbilityKey> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageAbilityKey message, MessageContext ctx) {

			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					if (message != null && ctx != null) {
						AbilityKeys key = message.key;
						SuperpowerPlayerHandler handler = SuperpowerHandler.getSuperpowerPlayerHandler(player);
						SuitSet suit = SuitSet.getSuitSet(player);
						Ability ability = key.type == AbilityKeyType.SUPERPOWER_ACTION ? (handler != null ? handler.getAbilityForKey(key) : null) : key.type == AbilityKeyType.ARMOR_ACTION && suit != null ? SuperpowerHandler.getSuperpowerData(player).getSuitSetAbilityHandler().getAbilityForKey(key) : null;
						
						if(MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Server(key, player, message.pressed)))
							return;
						
						if (message.pressed) {
							if (ability != null && ability.isUnlocked() && ability.checkConditions()) {

								if (ability.getAbilityType() == AbilityType.ACTION) {
									if (ability.hasCooldown()) {
										if (ability.getCooldown() == 0) {
											if(ability.action()) {
												ability.setEnabled(false);
												ability.setCooldown(ability.getMaxCooldown());
												if (player instanceof EntityPlayerMP)
													LCCriteriaTriggers.EXECUTE_ABILITY.trigger((EntityPlayerMP) player, ability.getAbilityEntry());
											}
										}
									} else {
										ability.action();

										if (player instanceof EntityPlayerMP)
											LCCriteriaTriggers.EXECUTE_ABILITY.trigger((EntityPlayerMP) player, ability.getAbilityEntry());
									}
								}

								else if (ability.getAbilityType() == AbilityType.TOGGLE) {
									ability.action();

									if (player instanceof EntityPlayerMP) {
										LCCriteriaTriggers.EXECUTE_ABILITY.trigger((EntityPlayerMP) player, ability.getAbilityEntry());
									}
								}

								else if (ability.getAbilityType() == AbilityType.HELD) {
									ability.setEnabled(true);

									if (player instanceof EntityPlayerMP)
										LCCriteriaTriggers.EXECUTE_ABILITY.trigger((EntityPlayerMP) player, ability.getAbilityEntry());
								}
							}
						} else {
							if (ability != null && ability.isUnlocked()) {
								if (ability.getAbilityType() == AbilityType.HELD) {
									ability.setEnabled(false);
								}
							}
						}
					}
				}

			});

			return null;
		}

	}

}
