package lucraft.mods.lucraftcore.superpowers.abilities;

import net.minecraft.entity.player.EntityPlayer;

public abstract class AbilityToggle extends Ability {

	public AbilityToggle(EntityPlayer player) {
		super(player);
		this.setEnabled(false);
		this.setAbilityType(AbilityType.TOGGLE);
	}

	@Override
	public abstract void updateTick();

	@Override
	public boolean action() {
		this.setEnabled(!isEnabled());
		return true;
	}

}