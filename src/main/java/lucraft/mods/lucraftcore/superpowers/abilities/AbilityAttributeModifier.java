package lucraft.mods.lucraftcore.superpowers.abilities;

import java.util.UUID;

import com.google.gson.JsonObject;

import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.text.TextFormatting;

public abstract class AbilityAttributeModifier extends AbilityConstant {

	protected float factor;
	protected float factor_multiplier = 1F;
	protected int operation;
	protected UUID uuid;
	protected boolean fixed;

	public AbilityAttributeModifier(EntityPlayer player, UUID uuid, float factor, int operation) {
		super(player);
		this.factor = factor;
		this.operation = operation;
		this.uuid = uuid;
	}

	@Override
	public void readFromAddonPack(JsonObject data, IAbilityContainer abilities) {
		super.readFromAddonPack(data, abilities);
		this.factor = JsonUtils.getFloat(data, "factor");
		this.operation = JsonUtils.getInt(data, "operation");
		this.uuid = UUID.fromString(JsonUtils.getString(data, "uuid"));
	}

	@Override
	public String getDisplayDescription() {
		return super.getDisplayDescription() + "\n \n" + TextFormatting.BLUE + (getOperation() == 0 ? "+" : "*") + getFactor();
	}

	public abstract IAttribute getAttribute();

	public UUID getModifierUUID() {
		return uuid;
	}

	public float getFactor() {
		return factor;
	}

	public void setFactor(float factor) {
		this.factor = factor;
	}

	public void setFactorMultiplier(float multiplier) {
		this.factor_multiplier = multiplier;
	}

	public int getOperation() {
		return operation;
	}

	public void setOperation(int operation) {
		this.operation = operation;
	}

	public void setValuesFixed(boolean fixed) {
		this.fixed = fixed;
	}

	@Override
	public void updateTick() {
		if (player.getAttributeMap().getAttributeInstance(getAttribute()).getModifier(getModifierUUID()) != null && (player.getEntityAttribute(getAttribute()).getModifier(getModifierUUID()).getAmount() != getFactor() || player.getEntityAttribute(getAttribute()).getModifier(getModifierUUID()).getOperation() != getOperation())) {
			player.getAttributeMap().getAttributeInstance(getAttribute()).removeModifier(getModifierUUID());
		}

		if (player.getAttributeMap().getAttributeInstance(getAttribute()).getModifier(getModifierUUID()) == null) {
			AttributeModifier modifier = new AttributeModifier(getModifierUUID(), getUnlocalizedName(), getFactor() * factor_multiplier, getOperation()).setSaved(false);
			player.getAttributeMap().getAttributeInstance(getAttribute()).applyModifier(modifier);
		}
	}

	@Override
	public void lastTick() {
		if (player.getAttributeMap().getAttributeInstance(getAttribute()).getModifier(getModifierUUID()) != null) {
			player.getAttributeMap().getAttributeInstance(getAttribute()).removeModifier(getModifierUUID());
		}
	}

	@Override
	public boolean showInAbilityBar() {
		return false;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		super.deserializeNBT(nbt);
		if (!fixed) {
			this.factor = nbt.getFloat("Factor");
			this.operation = nbt.getInteger("Operation");
		}
		this.uuid = UUID.fromString(nbt.getString("UUID"));
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound nbt = super.serializeNBT();
		nbt.setFloat("Factor", factor);
		nbt.setInteger("Operation", operation);
		nbt.setString("UUID", uuid.toString());
		return nbt;
	}

}
