package lucraft.mods.lucraftcore.superpowers.abilities;

import java.util.Arrays;
import java.util.Random;

import com.google.gson.JsonObject;

import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityPotionPunch extends AbilityToggle {

	protected Potion potion;
	protected int amplifier;
	protected int duration;
	protected int cooldown_;

	public AbilityPotionPunch(EntityPlayer player) {
		super(player);
	}

	public AbilityPotionPunch(EntityPlayer player, Potion potion, int amplifier, int duration, int cooldown) {
		super(player);
		this.potion = potion;
		this.amplifier = amplifier;
		this.duration = duration;
		this.cooldown_ = cooldown;
	}

	@Override
	public void readFromAddonPack(JsonObject data, IAbilityContainer abilities) {
		super.readFromAddonPack(data, abilities);
		this.potion = Potion.REGISTRY.getObject(new ResourceLocation(JsonUtils.getString(data, "potion")));
		this.amplifier = JsonUtils.getInt(data, "amplifier");
		this.duration = JsonUtils.getInt(data, "duration");
		this.cooldown_ = JsonUtils.getInt(data, "cooldown", 0);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		ItemStack stack = new ItemStack(Items.POTIONITEM);
		stack = PotionUtils.appendEffects(stack, Arrays.asList(new PotionEffect(potion, duration, amplifier)));
		float zLevel = Minecraft.getMinecraft().getRenderItem().zLevel;
		Minecraft.getMinecraft().getRenderItem().zLevel = -100.5F;
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, 0);
		Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(stack, 0, 0);
		GlStateManager.popMatrix();
		Minecraft.getMinecraft().getRenderItem().zLevel = zLevel;
	}
	
	@Override
	public String getDisplayDescription() {
		return super.getDisplayDescription() + "\n \n" + TextFormatting.BLUE + StringHelper.translateToLocal(this.potion.getName());
	}
	
	@Override
	public boolean hasCooldown() {
		return cooldown_ > 0;
	}
	
	@Override
	public int getMaxCooldown() {
		return cooldown_;
	}
	
	@Override
	public void updateTick() {
		spawnPotionParticles(1);
	}

	private void spawnPotionParticles(int particleCount) {
		int i = this.potion.getLiquidColor();
		Random rand = new Random();
		if (i != -1 && particleCount > 0) {
			double d0 = (double) (i >> 16 & 255) / 255.0D;
			double d1 = (double) (i >> 8 & 255) / 255.0D;
			double d2 = (double) (i >> 0 & 255) / 255.0D;

			for (int j = 0; j < particleCount; ++j) {
				this.player.world.spawnParticle(EnumParticleTypes.SPELL_MOB, player.posX + (rand.nextDouble() - 0.5D) * (double) player.width, player.posY + rand.nextDouble() * (double) player.height, player.posZ + (rand.nextDouble() - 0.5D) * (double) player.width, d0, d1, d2);
			}
		}
	}

	@Override
	public void onHurt(LivingHurtEvent e) {
		if(isEnabled()) {
			e.getEntityLiving().addPotionEffect(new PotionEffect(potion, duration, amplifier));
		}
	}

}
