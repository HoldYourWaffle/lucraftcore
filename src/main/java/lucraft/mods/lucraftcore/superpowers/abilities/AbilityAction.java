package lucraft.mods.lucraftcore.superpowers.abilities;

import net.minecraft.entity.player.EntityPlayer;

public abstract class AbilityAction extends Ability {

	public AbilityAction(EntityPlayer player) {
		super(player);
		this.setAbilityType(AbilityType.ACTION);
	}

	public abstract boolean action();

}
