package lucraft.mods.lucraftcore.superpowers.capabilities;

import java.util.LinkedList;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower.SuitSetAbilityHandler;
import lucraft.mods.lucraftcore.superpowers.effects.EffectTrail.EntityTrail;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface ISuperpowerCapability {
	
	public EntityPlayer getPlayer();
	
	public void setSuperpower(Superpower superpower);
	public void setSuperpower(Superpower superpower, boolean update);
	public Superpower getSuperpower();
	public SuperpowerPlayerHandler getSuperpowerPlayerHandler();
	public SuitSetAbilityHandler getSuitSetAbilityHandler();
	public boolean hasPlayedBefore();
	public void setHasPlayedBefore(boolean played);
	
	@SideOnly(Side.CLIENT)
	public LinkedList<EntityTrail> getTrailEntities();
	@SideOnly(Side.CLIENT)
	public void addTrailEntity(Entity entity);
	@SideOnly(Side.CLIENT)
	void removeTrailEntity(Entity entity);
	
	public void onUpdate(Phase phase);
	
	public NBTTagCompound writeNBT();
	public void readNBT(NBTTagCompound nbt);
	public void loadSuperpowerHandler();
	public void loadSuitSetHandler();
	public void syncToPlayer();
	public void syncToPlayer(EntityPlayer receiver);
	public void syncToAll();
	
}
