package lucraft.mods.lucraftcore.superpowers;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.capabilities.ISuperpowerCapability;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys.AbilityKeyType;

public class JsonSuperpowerPlayerHandler extends SuperpowerPlayerHandler {

	public JsonSuperpowerPlayerHandler(ISuperpowerCapability cap, Superpower superpower) {
		super(cap, superpower);
	}

	@Override
	public Ability getAbilityForKey(AbilityKeys key) {
		for(Ability ab : getAbilities()) {
			if(key.type == AbilityKeyType.SUPERPOWER_ACTION && key.id == ab.getKey()) {
				return ab;
			}
		}
		return super.getAbilityForKey(key);
	}
	
}
