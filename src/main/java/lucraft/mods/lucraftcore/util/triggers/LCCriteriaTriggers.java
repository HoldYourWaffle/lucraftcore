package lucraft.mods.lucraftcore.util.triggers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.advancements.ICriterionTrigger;

public class LCCriteriaTriggers {

	public static final GetSuperpowerTrigger GET_SUPERPOWER = new GetSuperpowerTrigger();
	public static final LoseSuperpowerTrigger LOSE_SUPERPOWER = new LoseSuperpowerTrigger();
	public static final ExecuteAbilityTrigger EXECUTE_ABILITY = new ExecuteAbilityTrigger();
	
	public static void init() {
		registerCriteriaTrigger(GET_SUPERPOWER);
		registerCriteriaTrigger(LOSE_SUPERPOWER);
		registerCriteriaTrigger(EXECUTE_ABILITY);
	}
	
	public static void registerCriteriaTrigger(ICriterionTrigger trigger) {
		for(Method m : CriteriaTriggers.class.getDeclaredMethods()) {
			if(m.getName().equalsIgnoreCase("register") || m.getName().equalsIgnoreCase("func_192118_a")) {
				m.setAccessible(true);
				try {
					m.invoke(null, trigger);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
