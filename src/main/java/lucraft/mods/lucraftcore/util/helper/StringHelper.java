package lucraft.mods.lucraftcore.util.helper;

import java.text.SimpleDateFormat;

import net.minecraft.util.text.translation.I18n;

public class StringHelper {

	public static String unlocalizedToResourceName(String unlocalizedName) {
		String s = "";

		for (int i = 0; i < unlocalizedName.toCharArray().length; i++) {
			char c = unlocalizedName.toCharArray()[i];

			if (Character.isUpperCase(c))
				s = s + "_";

			s = s + Character.toLowerCase(c);
		}

		return s;
	}

	public static String translateToLocal(String string) {
		return I18n.translateToLocal(string);
	}
	
	public static String translateToLocal(String string, Object ... parameters) {
		return I18n.translateToLocalFormatted(string, parameters);
	}
	
	public static String intToTime(int time) {
		SimpleDateFormat df = new SimpleDateFormat("mm:ss");
		String formatted = df.format(time * 1000);
		return formatted;
	}
	
}
