package lucraft.mods.lucraftcore.util.helper;

import java.util.ArrayList;
import java.util.List;

import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.SPacketCustomSound;
import net.minecraft.network.play.server.SPacketParticles;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PlayerHelper {

	public static void playSound(World world, EntityPlayer player, double x, double y, double z, SoundEvent sound, SoundCategory category) {
		playSound(world, player, x, y, z, sound, category, 1F, 1F);
	}

	public static void playSound(World world, EntityPlayer player, double x, double y, double z, SoundEvent sound, SoundCategory category, float volume, float pitch) {
		if (player instanceof EntityPlayerMP) {
			((EntityPlayerMP) player).connection.sendPacket(new SPacketCustomSound(sound.getRegistryName().toString(), category, x, y, z, volume, pitch));
		}
	}

	public static void playSoundToAll(World world, double x, double y, double z, double range, SoundEvent sound, SoundCategory category) {
		playSoundToAll(world, x, y, z, range, sound, category, 1, 1);
	}

	public static void playSoundToAll(World world, double x, double y, double z, double range, SoundEvent sound, SoundCategory category, float volume, float pitch) {
		AxisAlignedBB a = new AxisAlignedBB(new BlockPos(x - range, y - range, z - range), new BlockPos(x + range, y + range, z + range));
		for (EntityPlayer players : world.getEntitiesWithinAABB(EntityPlayer.class, a)) {
			if (players instanceof EntityPlayerMP) {
				((EntityPlayerMP) players).connection.sendPacket(new SPacketCustomSound(sound.getRegistryName().toString(), category, x, y, z, volume, pitch));
			}
		}
	}
	
	public static void spawnParticle(EntityPlayer player, EnumParticleTypes particleIn, boolean longDistanceIn, float xIn, float yIn, float zIn, float xOffsetIn, float yOffsetIn, float zOffsetIn, float speedIn, int countIn, int... argumentsIn) {
		if(player instanceof EntityPlayerMP) {
			((EntityPlayerMP) player).connection.sendPacket(new SPacketParticles(particleIn, longDistanceIn, xIn, yIn, zIn, xOffsetIn, yOffsetIn, zOffsetIn, speedIn, countIn, argumentsIn));
		}
	}
	
	public static void spawnParticleForAll(World world, double range, EnumParticleTypes particleIn, boolean longDistanceIn, float xIn, float yIn, float zIn, float xOffsetIn, float yOffsetIn, float zOffsetIn, float speedIn, int countIn, int... argumentsIn) {
		AxisAlignedBB a = new AxisAlignedBB(new BlockPos(xIn - range, yIn - range, zIn - range), new BlockPos(xIn + range, yIn + range, zIn + range));
		for (EntityPlayer players : world.getEntitiesWithinAABB(EntityPlayer.class, a)) {
			spawnParticle(players, particleIn, longDistanceIn, xIn, yIn, zIn, xOffsetIn, yOffsetIn, zOffsetIn, speedIn, countIn, argumentsIn);
		}
	}

	public static void givePlayerItemStack(EntityPlayer player, ItemStack stack) {
		if (player.getHeldItemMainhand().isEmpty())
			player.setItemStackToSlot(EntityEquipmentSlot.MAINHAND, stack);
		else if (!player.inventory.addItemStackToInventory(stack)) {
			player.dropItem(stack, true);
		}
	}

	public static void teleportToDimension(EntityPlayer player, int dimension, double x, double y, double z) {
		int oldDimension = player.world.provider.getDimension();
		EntityPlayerMP entityPlayerMP = (EntityPlayerMP) player;
		MinecraftServer server = ((EntityPlayerMP) player).world.getMinecraftServer();
		WorldServer worldServer = server.getWorld(dimension);
		player.addExperienceLevel(0);

		if (player.dimension != dimension)
			worldServer.getMinecraftServer().getPlayerList().transferPlayerToDimension(entityPlayerMP, dimension, new CustomTeleporter(worldServer, x, y, z));
		player.setPositionAndUpdate(x, y, z);
		if (oldDimension == 1) {
			player.setPositionAndUpdate(x, y, z);
			worldServer.spawnEntity(player);
			worldServer.updateEntityWithOptionalForce(player, false);
		}
	}

	public static ItemStack getItemStackInHandSide(EntityLivingBase entity, EnumHandSide side) {
		if (side == EnumHandSide.RIGHT) {
			return entity.getPrimaryHand() == EnumHandSide.RIGHT ? entity.getHeldItemMainhand() : entity.getHeldItemOffhand();
		} else {
			return entity.getPrimaryHand() == EnumHandSide.LEFT ? entity.getHeldItemMainhand() : entity.getHeldItemOffhand();
		}
	}

	public static List<ItemStack> setSuitOfPlayer(EntityPlayer player, SuitSet suit) {
		List<ItemStack> list = new ArrayList<ItemStack>();
		if (suit.getHelmet() != null) {
			if (!player.getItemStackFromSlot(EntityEquipmentSlot.HEAD).isEmpty())
				list.add(player.getItemStackFromSlot(EntityEquipmentSlot.HEAD));
			player.setItemStackToSlot(EntityEquipmentSlot.HEAD, new ItemStack(suit.getHelmet()));
		}
		if (suit.getChestplate() != null) {
			if (!player.getItemStackFromSlot(EntityEquipmentSlot.CHEST).isEmpty())
				list.add(player.getItemStackFromSlot(EntityEquipmentSlot.CHEST));
			player.setItemStackToSlot(EntityEquipmentSlot.CHEST, new ItemStack(suit.getChestplate()));
		}
		if (suit.getLegs() != null) {
			if (!player.getItemStackFromSlot(EntityEquipmentSlot.LEGS).isEmpty())
				list.add(player.getItemStackFromSlot(EntityEquipmentSlot.LEGS));
			player.setItemStackToSlot(EntityEquipmentSlot.LEGS, new ItemStack(suit.getLegs()));
		}
		if (suit.getBoots() != null) {
			if (!player.getItemStackFromSlot(EntityEquipmentSlot.FEET).isEmpty())
				list.add(player.getItemStackFromSlot(EntityEquipmentSlot.FEET));
			player.setItemStackToSlot(EntityEquipmentSlot.FEET, new ItemStack(suit.getBoots()));
		}

		return list;
	}

	@SideOnly(Side.CLIENT)
	public static boolean hasSmallArms(EntityPlayer player) {
		if(player instanceof AbstractClientPlayer)
			return ((AbstractClientPlayer)player).getSkinType().equalsIgnoreCase("slim");
		return false;
	}
	
}
