package lucraft.mods.lucraftcore.test;

import lucraft.mods.lucraftcore.extendedinventory.IItemExtendedInventory;
import lucraft.mods.lucraftcore.util.items.ItemBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemTestExtendedInventory extends ItemBase implements IItemExtendedInventory {

	public ItemTestExtendedInventory() {
		super("test");
	}

	@Override
	public ExtendedInventoryItemType getEIItemType(ItemStack stack) {
		return ExtendedInventoryItemType.NECKLACE;
	}

	@Override
	public void onWornTick(ItemStack stack, EntityPlayer player) {
		
	}

	@Override
	public void onEquipped(ItemStack itemstack, EntityPlayer player) {
		System.out.println("EQUIP");
	}

	@Override
	public void onUnequipped(ItemStack itemstack, EntityPlayer player) {
		System.out.println("UNEQUIP");
	}

	@Override
	public void onPressedButton(ItemStack itemstack, EntityPlayer player, boolean pressed) {
		System.out.println("BUTTON");
	}

	@Override
	public boolean canEquip(ItemStack itemstack, EntityPlayer player) {
		return true;
	}

	@Override
	public boolean canUnequip(ItemStack itemstack, EntityPlayer player) {
		return true;
	}
	
}
